#include <vector>
#include <cstring>
#include <iostream>
#include <map>
#include <sstream>
#include <fstream>
#include <cmath>
#include <algorithm>

struct Size
{
	unsigned int width;
	unsigned int height;
};

struct Node
{
	int x;
	int y;
	char item;

	std::string toString() const {
		std::stringstream sstr;
		sstr << "(x : " << x << ", y : " << y << ", item : " << item << ")";
		return sstr.str();
	}

	bool operator==(const Node& node) const {
		return (node.x == x && node.y == y);
	}
};

void removeNodeFromList(std::vector<Node>& nodeList, const Node& node) 
{
	nodeList.erase(std::remove(nodeList.begin(), nodeList.end(), node), nodeList.end());
}

int getManhattanDistance(const Node& start, const Node& goal) 
{	
	return abs(goal.x - start.x) + abs(goal.y - start.y);
}

int getHeuristicCost(const Node& start, const Node& goal) 
{
	return getManhattanDistance(start, goal);
}

class Graph
{
public:
	Graph() {};

	void setSize(unsigned int w, unsigned int h) { this->size.width = w; this->size.height = h; };
	Size getSize() const { return size; };

	Node getFirstNodeWithItem(char c) const {
		for (const Node& n : nodeList) {
			if (n.item == c) return n;
		}
		return (Node){-1, -1, '0'};
	}

	Node getNodeAt(int x, int y) const { // Returns Node at (x, y). If nothing found, return node with (x, y) and 0 as item value.
		for (const Node& n : nodeList) {
			if (n.x == x && n.y == y) return n;
		}
		return (Node){-1, -1, '0'};
	};

	bool nodeExists(const Node& node) const {
		for (const Node& n : nodeList) {
			if (n.x == node.x && n.y == node.y) return true;
		}
		return false;
	};

	bool addNode(const Node& node) {
		if (!nodeExists(node)) {
			nodeList.push_back(node);
			return true;
		}
		return false;
	};

	bool removeNode(const Node& node) {
		if (nodeExists(node)) {
			removeNodeFromList(nodeList, node);
			return true;
		}
		return false;
	};

	bool loadFromFile(const std::string& fileName) {
		// Load map elements from file
		std::ifstream file(fileName);
		std::string line;
		int y = 0;
		while (std::getline(file, line)) {
			std::istringstream iss(line);
			int x = 0;
			char c;
			while (iss >> c) {
				addNode({x, y, c});
				x++;
			}
			if (y == 0) {
				size.width = x;
			}
			size.height++;
			y++;
		}
		return true;
	}

	void display() {
		char buffer[size.width][size.height];
		memset(buffer, '0', sizeof(buffer[0][0]) * size.width * size.height);
		for (const Node& node : nodeList) {
			buffer[node.x][node.y] = node.item;
		}
		for (int j=0; j<size.height; ++j) {
			for (int i=0; i<size.width; ++i) {
				putchar(buffer[i][j]);
			}
			putchar('\n');
		}
	}

private:
	Size size = {0, 0};
	std::vector<Node> nodeList;
};

// Returns the node with the lowest F score (walked+estimated) using provided node list and nodes F scores map
Node getNodeWithLowestFScore(const std::vector<Node>& nodes, const std::map<std::string, int>& nodesFScores)
{
	Node bestNode;
	int lowestFScore = -1;
	for (Node n : nodes) {
		double nodeScore = INFINITY;
		if (nodesFScores.find(n.toString()) != nodesFScores.end()) {
			nodeScore = nodesFScores.at(n.toString());
		}
		if (nodeScore < lowestFScore || lowestFScore == -1) {
			lowestFScore = nodeScore;
			bestNode = n;
		}
	}
	return bestNode;
}

// Returns all neighbours (vector of nodes) of currentNode, based on provided graph. '1' are obstacles. The rest is walkable.
std::vector<Node> getNeighboursOfNode(const Graph& graph, const Node& currentNode) 
{
	std::vector<Node> neighbours;
	for (int dx=-1; dx<=1; ++dx) {
		for (int dy=-1; dy<=1; ++dy) {
			const Node& otherNode = graph.getNodeAt(currentNode.x + dx, currentNode.y + dy); // Invalid nodes (x: -1, y: -1) can be returned
			if (currentNode == otherNode) continue; // Node is not its own neighbour
			if (otherNode.item == '1') continue; // Obstacles are not neighbours
			if (otherNode.x < 0 || otherNode.y < 0) continue; // Nodes outside graph boundaries are invalid
			neighbours.push_back(otherNode);
		}
	}
	return neighbours;
}

// Returns whether nodeList contains node
bool nodeInList(const std::vector<Node>& nodeList, const Node& node) 
{
	for (const Node& n : nodeList) {
		if (n == node) return true;
	}
	return false;
}

// Builds a path (vector of nodes) based on cameFrom map.
std::vector<Node> reconstructPath(const std::map<std::string, Node>& cameFrom, const Node& goal) 
{
	std::vector<Node> path;
	path.push_back(goal); // Reconstruct from the end
	Node current = goal;
	while (cameFrom.find(current.toString()) != cameFrom.end()) {
		current = cameFrom.at(current.toString());
		path.push_back(current);
	}
	return path;
}

// Returns path computed using provided graph and start/goal nodes. 
std::vector<Node> astar(const Graph& graph, const Node& start, const Node& goal)
{
	// Init containers
	std::vector<Node> openSet, closedSet;
	std::map<std::string, Node> cameFrom; // <node tostring, node which it came from>
	std::map<std::string, int> nodesGScores; // <node tostring, node's gscore> infinity+ when unset
	std::map<std::string, int> nodesFScores; // <node tostring, node's fscore> infinity+ when unsete

	// Init first node we iterate on (start)
	openSet.push_back(start);
	nodesGScores[start.toString()] = 0; // Start's G score is 0 since we haven't moved from start yet
	nodesFScores[start.toString()] = getHeuristicCost(start, goal); // Start's F score is purely based on estimation
	while (!openSet.empty()) {
		const Node& current = getNodeWithLowestFScore(openSet, nodesFScores);
		if (current == goal) return reconstructPath(cameFrom, current);
		removeNodeFromList(openSet, current);
		closedSet.push_back(current);
		std::vector<Node> neighbours = getNeighboursOfNode(graph, current);
		for (const Node& neighbour : neighbours) {
			if (nodeInList(closedSet, neighbour)) continue; // Don't operate on nodes already evaluated
			if (!nodeInList(openSet, neighbour)) { // Discover a new neighbour node
				openSet.push_back(neighbour);
				int gScoreAttempt = nodesGScores[current.toString()] + getManhattanDistance(current, neighbour); // G = walked
				double gScoreNeighbour = (nodesGScores.find(neighbour.toString()) != nodesGScores.end() ? 
					nodesGScores.at(neighbour.toString()) : 
					INFINITY);
				if (gScoreAttempt >= gScoreNeighbour) continue; // Not a better path, skip
				cameFrom[neighbour.toString()] = current;
				nodesGScores[neighbour.toString()] = gScoreAttempt;
				nodesFScores[neighbour.toString()] = nodesGScores.at(neighbour.toString()) + getHeuristicCost(neighbour, goal); // F = walked + estimation
			}
		}
	}
	return std::vector<Node>();
}

int main(int argc, char* argv[])
{
	// Init stuff and display loaded map
	printf("A* pathfinding\n\n");
	const std::string fileName = "world.txt";
	Graph graph;
	graph.loadFromFile(fileName);
	graph.display();

	// Get start and goal nodes
	const Node& startNode = graph.getFirstNodeWithItem('S');
	const Node& goalNode = graph.getFirstNodeWithItem('G');

	// Get path to goal using A* algorithm
	std::cout << std::endl << "Computing path..." << std::endl << std::endl;
	std::vector<Node> path = astar(graph, startNode, goalNode);

	// Display path as '+' characters
	for (const Node& node : path) {
		if (node.item == '0') {
			graph.removeNode(node);
			graph.addNode((Node){node.x, node.y, '+'});
		}
	}
	graph.display();

	return 0;
}